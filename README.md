# ATD

ATD is a new web development backend framework for Ruby. It is built on top of rack, and is meant to be small easy and light. That is why it provides a very minimalist way of writing your backend. It treats a request as a request, without splitting it based on the HTTP method. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'atd'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install atd

## Usage

### Setup

Setup is as easy as `require "atd"` at the top of whatever file you need it in

### Routing

#### Basic Routing

On the lowest level it can be used serve files or any strings when a path is requested (and optionally when a specific HTTP method is used). Here is a basic route that will serve `Hello World` when `/` is sent a HTTP request:

```ruby
request "/", "Hello World"
```

The `request` method is also alias to `req` and `r` for better code readability if you prefer. Here is how you could use the same basic routing syntax to return `Hello World` for a basic HTTP GET request to `/`:

```ruby
req.get "/", "Hello World"
```

This will only respond to HTTP GET requests, whereas the first example will respond to any HTTP request.

#### Advanced Routing

You can also add blocks to the basic routing methods to execute code when they are reached, and customize the response to the incoming request. Here is an example that will return the HTTP method used to request `/`:

```ruby
r "/" do
  @http[:output] = @http[:method]
end
```

This example uses the `@http` variable which is provided to the route when called. You could also run some complex method created outside the route. This route is called in the same scope as any block you were to declare. Here is an example:

```ruby
r "/", "I'm computing a complex function" do
  complex_function
end
```

##### @http

The `@http` instance variable can be used to work with the http request the the route is parsing. Here are some ways you can use the `@http` variable:

```ruby
@http[:status_code] = 200 # By default the status code is 200. You can manipulate this.
@http[:headers] = {} # By defualt there are no headers. You can add whatever you want.
@http[:method] = env["REQUEST_METHOD"] # Writing to this does nothing. Just a way for you to see the request method.
@http[:output] # This is set to the output you give in the args for request
@http[:request] # The associated Rack::Request for the request.
```

### Apps

ATD also allows you to use different "apps". Each app is independent of the others and lives is a class with the same name as the app. A file can have any number of apps, each of which can have it's own settings, files, and routes. By default, adding routes to `main` will make them a part of the app `DefaultApp`, which will work fine if you only need one app.

#### App Creation

To create a route you can use `ATD.new("AppName")`. It is important to note that *ATD.new is not a construtor*. It simply behaves like one because you can use it to "construct" a new app. The app creation process creates a new class which you can open anywhere in your app file, with the name you pass. The name must be a string or symbol, and must be a valid class name. You must call the constructor before you begin adding routes to the class, or open the class at all.

#### App Routing

To add routes to an app, you simply must call the standard routing method inside the app class. For example, to create and add routes to an app called `Name` I would use the following code:

```ruby
ATD.new(:Name)
class Name
  request "/", "Hello World"
end
```

### Logging

Currently there is no specified logging interface. Just use `puts` or `p` to log to `STDOUT`.

### Middleware

Middleware is currently used inside the app with the `ATD::Middleware` module. Currently use this with caution, backwards compatibility may be broken. It is for use only internally for now. For documentation, look to the comments.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/izwick-schachter/atd/issues. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.
