# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'atd/version'

Gem::Specification.new do |spec|
  spec.name          = "atd"
  spec.version       = ATD::VERSION
  spec.authors       = ["ACecretMaster"]
  spec.email         = ["izwick.schachter@gmail.com"]

  spec.summary       = 'The assistant technical director of your website. It does the dirty work so you can see the big picture.'
  spec.homepage      = "https://rubygems.org/gems/atd"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  raise "RubyGems 2.0 or newer is required to protect against public gem pushes." unless spec.respond_to?(:metadata)
  spec.metadata['allowed_push_host'] = "https://rubygems.org"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }+"test/assets/test.html"
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "rack", "~> 2.0"

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "minitest-reporters", "~> 1.1"
  spec.add_development_dependency "rubocop", "~> 0"
  spec.add_development_dependency "yard", "~> 0.9"
  spec.add_development_dependency "rack-test", "~> 0.6"
end
