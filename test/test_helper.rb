$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
ENV['RACK_ENV'] = 'test'
require 'atd'
require 'minitest/autorun'
require 'minitest/assertions'
require 'minitest/reporters'
require 'rack/test'

MiniTest::Reporters.use!
