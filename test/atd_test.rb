require_relative 'test_helper'

class TestClass
  class <<self
    attr_accessor :get, :post, :put, :patch, :delete
  end
end

ATD.new("Name")

class Name
  req.get "/", "Gotten"
  r.post "/", "Posted"
  request.put "/", "Putsed"
  r.delete "/", "Deleted"
  r.patch "/", "Patched"
end

class Name
  r.get "/block-test", "hi" do
    TestClass.get = true
  end

  r.get "/editing-test", "Before" do
    @http[:output] = "#{@http[:output]} After"
  end

  r.post "/params-test" do
    @http[:output] = @http[:request].params["test"]
  end

  r.get "/html-test", File.new(File.join(__dir__,"assets/test.html")) # TODO: Make it possible to just say "test.html" even when called from ouside dir.

  r "/all-methods" do
    @http[:output] = @http[:method]
  end
end

describe ATD do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_that_it_has_a_version_number
    refute_nil ::ATD::VERSION
  end

  def test_it_does_something_useful
    assert true
  end
end

describe ATD::Route do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_request_creation
    assert_instance_of ATD::Route, req
    assert_instance_of ATD::Route, r
  end

  def test_route_creation
    assert ATD::Route.new.get("/").same_properties_as? r.get("/")
  end

  def test_plain_routes
    assert_equal "Gotten", get("/").body
    assert_equal "Posted", post("/").body
    assert_equal "Deleted", delete("/").body
    assert_equal "Putsed", put("/").body
    assert_equal "Patched", patch("/").body
  end

  def test_block
    get "/block-test"
    assert_equal true, TestClass.get
  end

  def test_all_methods_routes
    assert_equal "get", get("/all-methods").body.downcase
  end
end

describe ATD::App do
  include Rack::Test::Methods

  def app
    Name.new
  end

  def test_creation
    assert_instance_of Class, ATD.new("Name2")
  end

  def test_template
    assert Name.new.respond_to?("call")
  end

  def test_template_is_rack_app
    responce = Name.new.call("env")
    assert_instance_of Array, responce
    assert_instance_of Fixnum, responce[0]
    assert_instance_of Hash, responce[1]
    assert_instance_of Array, responce[2]
  end

  def test_block_output_editing
    assert_equal "Before After", get("/editing-test").body
  end

  def test_404_error
    response = get("/ajeoifjaoigh")
    assert_equal "Error 404", response.body
    assert_equal 404, response.status
  end

  def test_recieve_params
    response = post("/params-test", "test" => "hi")
    assert_equal "hi", response.body
  end

  def test_read_html
    assert_equal File.read(File.join(__dir__,"./assets/test.html")), get("/html-test").body
  end
end
