# @!visibility private
class Hash
  # Not only merges two hashes, but also merges the hashes that may be nested in.
  #
  # For example:
  #   {a: {b: "c"}}
  # Is a nested hash
  def deep_merge(second)
    merger = proc do |_, v1, v2|
      if v1.is_a?(Hash) && v2.is_a?(Hash) then v1.merge(v2, &merger)
      elsif v1.is_a?(Array) && v2.is_a?(Array) then v1 | v2
      elsif [:undefined, nil, :nil].include?(v2) then v1
      else v2
      end
    end
    merge(second.to_h, &merger)
  end
end

# This method only exists for the test suite, specifically {ATDTest#test_route_creation}.
# @!visibility private
class Object
  # Checks if two objects are instances of the same class and that they have the same instance variables
  def same_properties_as?(other_class)
    other_class.class == self.class && class_instance_variables == other_class.class_instance_variables
  end

  # Returns the instance variables of a class
  def class_instance_variables
    instance_variables.map { |var| [var, instance_variable_get(var)] }.to_h
  end
end
