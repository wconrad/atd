# All middleware must respond_to?("setup")
# To register middleware, after the class call ATD::Middleware.register(Your::Middleware)
module ATD
  module Middleware
    @middleware = {}
    @sources = []

    def self.register(source)
      @sources.push source
    end

    def self.run(target = nil)
      return nil if @middleware[target.class].nil?
      @middleware[target.class].each do |action|
        action.call(target)
      end
    end

    def self.create(source, target)
      @middleware[target] = [] if @middleware[target].nil?
      @middleware[target].push(source)
    end

    def self.setup(target)
      # At some point, target should be used so that middleware are only setup on one target.
      @sources.each(&:setup)
      target
    end
  end
end
