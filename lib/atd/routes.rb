
module ATD
  module Compilation
    def compile
    end

    def self.precompile(route)
      output = route.output
      name = output
      return nil if output.nil?
      if output.include?(".") || output.is_a?(File)
        file = File.read(File.join("./assets/#{output}")) if output.class.is_a? String
        file = File.read(output) if output.is_a? File
        return nil if file.nil?
        unless output.is_a? File
          output = output.split(".")
          name = output.shift
          output.each do |extension|
            file = Precompiler.send(extension, file)
          end
        end
        route.output = file
      end
      name
    end

    module Precompiler
      module_function

      def html(file)
        file
      end
    end

    def self.setup
      ATD::Middleware.create(ATD::Compilation.method(:precompile), ATD::Route)
    end
  end
end

ATD::Middleware.register(ATD::Compilation)
